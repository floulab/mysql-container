FROM mysql:8
# remember to use: --default-authentication-plugin=mysql_native_password
# in ansible docker_container module

MAINTAINER Ioannis Angelakopoulos<ioagel@gmail.com>

ENV UID=10002 GID=10002

RUN groupmod -g $GID mysql && \
    usermod -u $UID mysql && \
    chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
