### What is this Image for? ###

I wanted the **mysql** *user* and *group* to match between the host and the docker
container.
That happens when the *uid* and *gid* are the same.

This image is based on mysql:5.7 and updates, when mysql official image gets updated.
